import { useAuthStore } from '~/stores/auth'

export default defineNuxtRouteMiddleware(() => {
  const { isAuthenticated } = useAuthStore()

  if (user) {
    return navigateTo({
      path: '/',
    })
  }
})
