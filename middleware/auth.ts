import { useAuthStore } from '~/stores/auth'

export default defineNuxtRouteMiddleware((to) => {
  const { isAuthenticated } = useAuthStore()

  if (!isAuthenticated) {
    return navigateTo({
      path: '/auth/login',
      query: {
        to: to.fullPath,
      },
    })
  }
})
