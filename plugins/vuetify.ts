import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

export default defineNuxtPlugin((nuxtApp) => {
  const vuetify = createVuetify({
    ssr: true,
    components,
    directives,
    theme: {
      themes: {
        light: {
          dark: false,
          colors: {
            primary: '#E42E2C',
            secondary: '#5ABBAD',
          },
        },
      },
    },
  })

  nuxtApp.vueApp.use(vuetify)
})
