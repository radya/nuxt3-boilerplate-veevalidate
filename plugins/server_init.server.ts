import { defineNuxtPlugin } from '#app'
import { useAuthStore } from '@/stores/auth'

export default defineNuxtPlugin(async ({ $pinia }) => {
  if (!process.server) return

  const { token, checkCredentials } = useAuthStore($pinia)

  if (token) {
    await checkCredentials()
  }
})
