const API_ENDPOINT = {
  AUTH: {
    LOGIN: '/api/v1/oauth/sign_in',
    LOGOUT: '/api/v1/oauth/revoke',
    PROFILE: '/api/v1/profile/me',
  },
}

export default API_ENDPOINT
