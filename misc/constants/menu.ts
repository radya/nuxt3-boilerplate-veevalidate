export default [
  {
    name: 'AUTHORIZED APPLICATIONS',
    url: '/authorized_applications',
    icon: 'authorized_application',
  },
  {
    name: 'LOGIN HISTORY',
    url: '/login_history',
    icon: 'login_history',
  },
  {
    name: 'LIST APPLICATION',
    url: '/application_lists',
    icon: 'application_lists',
  },
]
