import { defineStore } from 'pinia'
import { UserDetail } from '~/types/responses/auth/user_response_type'
import API_ENDPOINT from '~/misc/constants/api_endpoint'

interface AuthStorePropeties {
  token: string | null
  user: UserDetail | null
  loading: boolean
  fetched: boolean
}

export const useAuthStore = defineStore('auth', {
  state: (): AuthStorePropeties => {
    const accessToken = useCookie('access_token', { default: undefined })

    return {
      token: accessToken.value,
      user: null,
      loading: false,
      fetched: false,
    }
  },
  getters: {
    isAuthenticated: (state) => !!state.user,
  },
  actions: {
    setUser(user: UserDetail | null) {
      this.user = user
      this.fetched = true
    },
    setToken(token: string | null) {
      const accessToken = useCookie('access_token')
      accessToken.value = token
      this.token = token
    },
    async checkCredentials() {
      this.loading = true
      const config = useRuntimeConfig()

      try {
        const host = process.server ? config.apiUrl : ''

        const data = await $fetch<UserDetail>(
          host + API_ENDPOINT.AUTH.PROFILE,
          {
            headers: {
              Authorization: `Bearer ${this.token}`,
            },
          }
        )
        this.setUser(data)
      } catch (e) {
        this.setUser(null)
        this.setToken(null)
      } finally {
        this.loading = false
      }
    },
  },
})
