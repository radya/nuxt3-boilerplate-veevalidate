import { UseFetchOptions } from '#app'
import { NitroFetchRequest } from 'nitropack'
import { KeyOfRes } from 'nuxt/dist/app/composables/asyncData'

export function useHttp<T>(
  request: NitroFetchRequest,
  opts?:
    | UseFetchOptions<
        T extends void ? unknown : T,
        (res: T extends void ? unknown : T) => T extends void ? unknown : T,
        KeyOfRes<
          (res: T extends void ? unknown : T) => T extends void ? unknown : T
        >
      >
    | undefined
) {
  return useFetch<T>(request, {
    ...opts,
    onRequest({ options }) {
      const accessToken = useCookie('access_token', { default: undefined })

      if (accessToken.value) {
        options.headers = (options.headers || {}) as { [key: string]: string }
        options.headers.authorization = `Bearer ${accessToken.value}`
      }
    },
  })
}
