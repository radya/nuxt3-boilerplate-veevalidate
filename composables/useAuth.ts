import { useAuthStore } from '~/stores/auth'
import { LoginResponse } from '~/types/responses/auth/login_response_type'
import API_ENDPOINT from '~~/misc/constants/api_endpoint'

const useAuth = () => {
  const loading = ref<boolean>(false)
  const router = useRouter()

  const { setUser, token, setToken } = useAuthStore()

  const login = async (phone: string, password: string) => {
    const { data, pending, error } = await useHttp<LoginResponse>(
      API_ENDPOINT.AUTH.LOGIN,
      {
        method: 'POST',
        body: {
          phone,
          password: password,
          device_token: Math.floor(Math.random() * 100),
          latlong: '0,0',
          device_type: 2,
        },
      }
    )

    loading.value = pending.value

    if (error.value) {
      console.log(error.value)
    }

    const token = data.value?.data.user.access_token

    if (token) {
      setToken(token as string)

      await checkCredentials()

      router.push((query.to as string) || '/dashboard')
    }
  }

  const logout = () => {
    setUser(null)

    useHttp(API_ENDPOINT.AUTH.LOGOUT, {
      method: 'POST',
      body: {
        access_token: token,
        confirm: 1,
      },
    })
  }

  return {
    login,
    logout,
  }
}

export default useAuth
