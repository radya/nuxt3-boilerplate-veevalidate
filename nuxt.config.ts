import removeConsole from 'vite-plugin-remove-console'

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: [
    'vuetify/lib/styles/main.sass',
    '@mdi/font/css/materialdesignicons.min.css',
    '~/assets/styles/scss/main.scss',
  ],
  build: {
    transpile: ['vuetify'],
  },
  modules: [
    '@vueuse/nuxt',
    '@pinia/nuxt',
    '@nuxt-alt/proxy',
    '@sidebase/nuxt-auth',
  ],
  proxy: {
    proxies: {
      '/api': process.env.API_URL || 'http://pretest-qa.dcidev.id',
    },
  },
  vite: {
    plugins: [removeConsole()],
  },
  runtimeConfig: {
    apiUrl: process.env.API_URL || 'http://pretest-qa.dcidev.id',
    // Keys within public, will be also exposed to the client-side
    public: {
      siteTitle: process.env.SITE_TITLE || 'DCI Auth',
    },
  },
  imports: {
    // Auto-import pinia stores defined in `~/stores`
    dirs: ['stores'],
  },
})
