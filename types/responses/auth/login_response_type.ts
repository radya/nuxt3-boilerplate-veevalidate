export interface Token {
  access_token: string
  token_type: string
}

export interface LoginResponse {
  data: {
    user: Token
  }
}
