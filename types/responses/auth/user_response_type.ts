export interface UserDevice {
  device_token: string
  device_type: 'android' | 'ios' | 'web'
  device_status: string
}

export interface User {
  id: string
  phone: string
  user_status: string
  user_type: string
  sugar_id: string
  country: string
  latlong: string | null
  user_device: UserDevice
}

export interface UserEducation {
  school_name: string | null
  graduation_time: string | null
}

export interface UserCareer {
  company_name: string | null
  starting_from: string | null
  ending_in: string | null
}

export interface UserCover {
  url: string | null
}

export interface UserPicture {
  id: string
  picture: UserCover
}

export interface UserDetail {
  id: string
  name: string
  level: number
  age: number | null
  birthday: string | null
  gender: string | null
  zodiac: string | null
  hometown: string | null
  bio: string | null
  latlong: string | null
  education: UserEducation
  career: UserCareer
  user_pictures: UserPicture[]
  user_picture: UserPicture | null
  cover_picture: UserCover
}
